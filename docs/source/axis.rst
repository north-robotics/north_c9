==============
north\_c9.axis
==============

.. currentmodule:: north_c9.axis

.. py:module:: north_c9.axis

-------
Outputs
-------

The following classes can be used to control outputs on the North Robotics C9.

Output
------

.. autoclass:: Output
   :members:
   :undoc-members:

OpenOutput
----------

.. autoclass:: OpenOutput
   :members:
   :undoc-members:

----
Axes
----

The following classes can be used to control different types of axes connected to a C9.

Axis
----

.. autoclass:: Axis
   :members:
   :undoc-members:

RevoluteAxis
------------

.. autoclass:: RevoluteAxis
   :members:
   :undoc-members:

PrismaticAxis
-------------

.. autoclass:: PrismaticAxis
   :members:
   :undoc-members:

----------
Exceptions
----------

.. autoexception:: AxisError

.. autoexception:: AxisMoveError