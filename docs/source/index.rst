north_c9 - Control the North Robotics C9 and N9
===============================================

This package provides classes and utilities that can be used to control the North Robotics C9 controller and the
North Robotics N9 robot.

Contents
--------

.. toctree::
   :maxdepth: 3

   controller
   axis
   joysticks


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
