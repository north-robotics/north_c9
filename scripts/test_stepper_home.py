from north_c9.controller import C9Controller
from north_c9.axis import RevoluteAxis


c9 = C9Controller(address=1)

while True:
    print(c9.analog(0))

axis = RevoluteAxis(c9, axis=1, max_position=500, max_current=500, home_max_current=300, home_velocity_counts=10, home_acceleration_counts=50)
axis.home()
print('done')