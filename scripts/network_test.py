import time
import logging
from ftdi_serial import Serial
from north_c9.controller import C9Controller

logging.basicConfig(level=logging.INFO)


# DEVICE_SERIAL = 'FT2FVF6U'
DEVICE_SERIAL = 'A50285BI'
# CONTROLLER_ADDRESSES = [1, 2]
CONTROLLER_ADDRESSES = [2]

serial = Serial(device_serial=DEVICE_SERIAL)
controllers = [C9Controller(serial, address=address) for address in CONTROLLER_ADDRESSES]

for controller in controllers:
    print(controller.info())

while True:
    for controller in controllers:
        controller.info()