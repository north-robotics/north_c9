from tests.context import north_c9
from north_c9.protocol import *

OldC9Protocol = C9Protocol


class C9Protocol(OldC9Protocol):
    def echo(self) -> bytes:
        return b'<ECHOSUCKA>\r'

    def home(self) -> bytes:
        return b'<HOME>\r'

    def flag(self) -> bytes:
        return b'<0000000000>\r'

    def goax(self, axes: List[int], num_axes: int=C9_NUM_AXIS) -> bytes:
        return b'<GOAX>\r'

    def mopy(self, trajectory: List[int]) -> bytes:
        return b'<MOPY>\r'

    def ping(self, timeout: float=1.0, retries: int=5) -> bytes:
        return b'<ECHOSUCKA>\r'
