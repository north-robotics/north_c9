import unittest
from queue import Queue
from north_c9.emulator.serial import MockDevice, ClientDevice, ServerDevice
from ftdi_serial import Serial
from tests.util import *


def queue_contents(queue: Queue):
    return bytes([queue.get() for i in range(queue.qsize())])


class SerialTestSuite(unittest.TestCase):
    def test_mock_device(self):
        device = MockDevice()
        serial = Serial(device=device)

        serial.write(b'test')
        assert_equal(device.output.qsize(), 4)
        assert_equal(queue_contents(device.output), b'test')

        serial.disconnect()

    def test_client_server(self):
        server = ServerDevice()
        client = server.create_client()

        client_serial = Serial(device=client)
        server_serial = Serial(device=server)

        server_serial.write(b'testing')
        assert_equal(client_serial.in_waiting, 7)
        assert_equal(client_serial.read(), b'testing')

        client_serial.write(b'testing')
        assert_equal(server_serial.read(), b'testing')
        assert_equal(client_serial.in_waiting, 0)

        clients = [client_serial, Serial(device=server.create_client()), Serial(device=server.create_client())]
        server.write(b'client_test')
        for client in clients:
            assert_equal(client.read(), b'client_test')

        client_serial.write(b'test')
        assert_equal(server_serial.read(), b'test')
        for client in clients[1:]:
            assert_equal(client.read(), b'test')
        assert_equal(client_serial.read(), b'')