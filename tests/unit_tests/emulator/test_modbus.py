import unittest
from queue import Queue
import time
from north_c9.emulator.serial import MockDevice, ClientDevice, ServerDevice
from north_c9.emulator.modbus import ModbusNetwork, ModbusDevice, ModbusMessage
from ftdi_serial import Serial
from tests.util import *


MODBUS_MESSAGE = b'\x05\x04\x13\x88\x00\x02\xF4\xE1'


def queue_contents(queue: Queue):
    return bytes([queue.get() for i in range(queue.qsize())])


class ModbusTestSuite(unittest.TestCase):
    def test_thread(self):
        modbus = ModbusNetwork(Serial(device=MockDevice()))
        modbus.start()
        assert_equal(modbus.thread.is_alive(), True)
        modbus.stop()
        assert_equal(modbus.thread.is_alive(), False)

    def test_message_parse(self):
        message = ModbusMessage.parse(MODBUS_MESSAGE)
        assert_equal(message.address, 5)
        assert_equal(message.function, 4)
        assert_equal(message.data, b'\x13\x88\x00\x02')
        assert_equal(message.crc, b'\xf4\xe1')
        assert_equal(message.valid(), True)

    def test_modbus_device(self):
        registers = [0, 0, 0, 1]

        server = ServerDevice()
        server_serial = Serial(device=server)
        client = server.create_client()

        modbus_device = ModbusDevice(5, [(5000, registers)])
        modbus = ModbusNetwork(serial=Serial(device=client))
        modbus.add_device(modbus_device)
        modbus.start()

        server.write(MODBUS_MESSAGE)
        time.sleep(0.1)
        assert_equal(modbus_device.message_count, 1)
        assert_equal(server_serial.read(), b'\x05\x04\x04\x00\x00\x00\x01\x7f\x84')

        modbus.stop()