import unittest
import logging
import time
import matplotlib.pyplot as plt
from north_c9.emulator.serial import ServerDevice, ClientDevice, Serial
from north_c9.emulator.command_reader import CommandReader, Command
from tests.util import *

logging.basicConfig(level=logging.DEBUG)


class CommandReaderTestSuite(unittest.TestCase):
    def test_command_reader(self):
        server_device = ServerDevice()
        server_serial = Serial(device=server_device)
        client_device = server_device.create_client()
        client_serial = Serial(device=client_device)

        command_reader = CommandReader(client_serial)
        command_reader.start()

        server_serial.write(b'PING\r')

        command_reader.stop()