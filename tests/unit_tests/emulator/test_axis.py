import unittest
import time
import matplotlib.pyplot as plt
from north_c9.emulator.axis import trajectory_position
from tests.util import *


class AxisTestSuite(unittest.TestCase):
    def test_trajectory_position(self):
        times = []
        positions = []

        for t in range(0, 30):
            times.append(t)
            positions.append(trajectory_position(10.0, 1.0, 100.0, float(t)))

        plt.plot(times, positions)
        plt.show()

    def test_trajectory_negative_position(self):
        times = []
        positions = []

        for t in range(0, 30):
            times.append(t)
            positions.append(trajectory_position(10.0, 1.0, -100.0, float(t)))

        plt.plot(times, positions)
        plt.show()