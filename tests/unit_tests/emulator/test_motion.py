import unittest
import logging
import time
import matplotlib.pyplot as plt
from north_c9.emulator.serial import ServerDevice, ClientDevice, Serial
from north_c9.emulator.modbus import Modbus, ModbusDevice, ModbusNetwork
from north_c9.emulator.axis import Axis
from north_c9.emulator.simulator import Simulator
from north_c9.emulator.motion import AuxMotionAxis, Motion
from tests.util import *

logging.basicConfig(level=logging.DEBUG)


class MotionTestSuite(unittest.TestCase):
    def test_aux_motion_axis(self):
        server_device = ServerDevice()
        modbus = Modbus(Serial(device=server_device))

        modbus_network = ModbusNetwork(Serial(device=server_device.create_client()), [
            Axis(1), Axis(2), Axis(3), Axis(4)
        ])
        modbus_network.start()

        simulator = Simulator()
        simulator.start()

        motion = Motion(modbus)

        #time.sleep(1)

        print(motion.axis_position(0))

        motion.move_sync_relative([(0, 100), (1, 10), (2, 200), (3, 50)], 100, 100)
        time.sleep(1)
        while motion.axis_moving(0):
            for axis in range(4):
                print(motion.axis_position(axis))
            time.sleep(1)

        modbus_network.stop()
        simulator.stop()