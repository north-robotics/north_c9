from tests import mock_serial, mock_protocol
from north_c9.client import *

OldC9Client = C9Client


class C9Client(mock_protocol.C9Protocol, OldC9Client):
    def __init__(self,
                 connection: Serial = None,
                 device: Device = None,
                 device_serial: str = None,
                 device_number: int = None,
                 home: bool = True,
                 connect: bool = True,
                 verbose=False) -> None:
        mock_protocol.C9Protocol.__init__(self)
        OldC9Client.__init__(self, connection=mock_serial.Serial())

    def connect(self):
        pass

    def disconnect(self):
        pass

    def write_command(self, name: str, args: List[Any]=[], **kwargs):
        pass

    def request_command(self, name: str, args: List[Any]=[], timeout: Optional[float]=None, retries: int=0) -> bytes:
        return b''
