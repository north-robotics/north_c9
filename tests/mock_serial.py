from ftdi_serial import *

FtdiSerial = Serial


class Serial(FtdiSerial):
    def open_device(self):
        self.device = True
        pass

    def connect(self):
        self.connected = True

    def disconnect(self):
        self.connected = False

    def init_device(self):
        pass

    def update_timeouts(self):
        pass

    @property
    def info(self) -> SerialDeviceInfo:
        return SerialDeviceInfo(description='MOCK SERIAL DEVICE')

    @property
    def serial_number(self) -> str:
        return 'MOCK'

    @property
    def in_waiting(self) -> int:
        return len(self.input_buffer)

    @property
    def out_waiting(self) -> int:
        return 0

    def read(self, num_bytes: int=None, timeout: Optional[NumberType]=None) -> bytes:
        if not self.connected:
            raise SerialException('Cannot read, device is not connected')

        data = self.input_buffer
        self.input_buffer = b''

        return data

    def write(self, data: Union[bytes, str], timeout: Optional[NumberType] = None) -> int:
        return len(data)

    def flush(self):
        pass

    def reset_input_buffer(self):
        pass

    def reset_output_buffer(self):
        pass
