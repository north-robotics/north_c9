import time
import pywemo

DEVICE_URL = 'http://192.168.1.197:49153/setup.xml'
device = None


def get_device():
    global device

    if device is None:
        device = pywemo.discovery.device_from_description(DEVICE_URL, None)

    return device


def on(wait=True):
    switch = get_device()
    switch.on()
    if wait:
        time.sleep(15)


def off(wait=True):
    switch = get_device()
    switch.off()
    if wait:
        time.sleep(5)