import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1TwUHwPSHzjxD5xRhM4n7-YtBNbcsS3RVntzqjVrOMcs'

def get_sheets():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheets = service.spreadsheets()

    return sheets


def append_rows(id, sheet, rows):
    sheets = get_sheets()
    range = f'{sheet}!A:A'
    sheets.values().append(
        spreadsheetId=id,
        range=range,
        body={ 'majorDimension': 'ROWS', 'values': rows},
        valueInputOption="USER_ENTERED"
    ).execute()


def append_row(id, sheet, row):
    append_rows(id, sheet, [row])


if __name__ == '__main__':
    append_row('1TwUHwPSHzjxD5xRhM4n7-YtBNbcsS3RVntzqjVrOMcs', 'Sheet1', [1, 2, 3, 4])