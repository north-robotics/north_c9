import time
from datetime import datetime
import sqlite3
import codecs
from threading import Thread
from ftdi_serial import Serial, SerialReadTimeoutException


class ModbusRequest:
    def __init__(self, test_id: str, cycle: int, date: int, request: bytes, response: bytes):
        self.test_id = test_id
        self.cycle = cycle
        self.date = datetime.fromtimestamp(date)
        self.request = request
        self.response = response

    def __repr__(self):
        hex_request = codecs.encode(self.request or b'', 'hex_codec')
        hex_response = codecs.encode(self.response or b'', 'hex_codec')
        return f"ModbusRequest(date='{self.date.isoformat()}', request={hex_request}, response={hex_response}"

    def row(self):
        hex_request = codecs.encode(self.request or b'', 'hex_codec').decode()
        hex_response = codecs.encode(self.response or b'', 'hex_codec').decode()
        return [self.test_id, self.cycle, self.date.strftime('%Y-%m-%d %H:%M:%S.%f'), hex_request, hex_response]


class ModbusMonitor:
    def __init__(self, test_id, device_serial: str=None, database='modbus_messages.db', verbose=False):
        self.serial = Serial(device_serial=device_serial, read_timeout=0.01)
        self.database_path = database
        self.messages = []
        self.running = True
        self.test_id = test_id
        self.cycle = 0
        self.init_db()
        self.monitor_thread = Thread(target=self.monitor)
        self.verbose = verbose

    def init_db(self):
        database = sqlite3.connect(self.database_path)
        try:
            database.execute('''
                create table messages (test_id text, cycle integer, date integer, request blob, response blob)
            ''')
        except sqlite3.OperationalError:
            pass
        database.close()

    def save_frame(self, database, date, request=b'', response=b''):
        database.execute('insert into messages values (?, ?, ?, ?, ?)', (self.test_id, self.cycle, date, request, response))
        database.commit()

    def get_messages(self, database=None, number=10, close=False):
        if database is None:
            database = sqlite3.connect(self.database_path)
            close = True

        cursor = database.execute('select * from messages order by date desc limit ?', (number, ))
        messages = [ModbusRequest(*row) for row in cursor.fetchall()]

        if close:
            database.close()

        return messages

    def read_frame(self):
        frame = b''
        first = True
        while True:
            try:
                frame += self.serial.read(self.serial.in_waiting or 1)
                first = False
            except SerialReadTimeoutException:
                if not first and len(frame) > 10:
                    break

        try:
            response_index = frame.index(frame[0:2], 1)
            return frame[:response_index], frame[response_index:]
        except ValueError:
            return frame, None

    def monitor(self):
        database = sqlite3.connect(self.database_path)
        self.running = True
        while self.running:
            frame = self.read_frame()
            date = round(time.time())
            if self.verbose:
                print(ModbusRequest(self.test_id, self.cycle, date, *frame))
            self.save_frame(database, date, *frame)

        database.close()

    def start(self):
        self.monitor_thread.start()

    def stop(self):
        self.running = False
        self.monitor_thread.join(timeout=1000)
